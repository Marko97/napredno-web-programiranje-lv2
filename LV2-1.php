<?php
	$db_name = 'lv2'; 	
	$dir = "D:/Marko/Faks/5. Godina/Napredni Web/LV2/backup/$db_name";
	if (!is_dir($dir)) {
		if (!@mkdir($dir)) {
			die("<p>Direktorij se ne moze kreirati.</p>");
		}
	}
	$time = time();
	$dbc = @mysqli_connect('localhost', 'root', '', $db_name) OR 
	die("<p>Povezivanje na bazu nije moguce</p>");
	$r = mysqli_query($dbc, 'SHOW TABLES');
	if (mysqli_num_rows($r) > 0) {
		echo "<p>Backup od'$db_name'</p>";
		while (list($table) = mysqli_fetch_array($r, MYSQLI_NUM)) {
			$q = "SELECT * FROM $table";
			$r2 = mysqli_query($dbc, $q);
			$columns = $r2->fetch_fields();
			if (mysqli_num_rows($r2) > 0) {
				if ($fp = fopen ("$dir/{$table}_{$time}.txt", 'w9')) {
					while ($row = mysqli_fetch_array($r2, MYSQLI_NUM)) {
						fwrite($fp, "INSERT INTO $db_name (");
						foreach($columns as $column) {
							fwrite($fp, "$column->name");
							if ($column != end($columns)) {
								fwrite($fp, ", ");
							}
						}
						fwrite($fp, ")\r\nVALUES (");
						foreach ($row as $value) {
							$value = addslashes($value);
							fwrite ($fp, "'$value'");
							if ($value != end($row)) {
								fwrite($fp, ", ");
							} else {
								fwrite($fp, ")\";");
							}
						}
						fwrite ($fp, "\r\n");
					}
					fclose($fp);
					echo "<p>Tablica $table je spremljena.</p>";
					if ($fp2 = gzopen("$dir/{$table}_{$time}.sql.gz", 'w9')) {
						gzwrite($fp2, file_get_contents("$dir/{$table}_{$time}.txt"));
						gzclose($fp2);
					} else {
						echo "<p>File $dir/{$table}_{$time}.sql.gz se ne moze otvoriti.</p>";
						break;						
					}
				} else {
					echo "<p>File $dir/{$table}_{$time}.txt se ne moze otvoriti</p>";
					break;
				}
			}
		}
	} else {
		echo "<p>Baza nema niti jednu tablicu.</p>";
	}
	
?>